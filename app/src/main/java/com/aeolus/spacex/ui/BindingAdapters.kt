package com.aeolus.spacex.ui

import android.content.Intent
import android.net.Uri
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.aeolus.spacex.R
import com.aeolus.spacex.custom_views.ChartElementView
import com.aeolus.spacex.entities.launch.Payload
import com.bumptech.glide.Glide
import java.util.*

class BindingAdapters {

    companion object {

        @JvmStatic
        @BindingAdapter("imageUrl")
        fun setImageUrl(imageView: ImageView, url: String?) {
            imageView.setImageDrawable(null)
            imageView.invalidate()
            if (url == null)
                imageView.setImageDrawable(imageView.context.getDrawable(R.drawable.ic_rocket))
            else
                Glide.with(imageView.context).load(url).into(imageView)
        }

        @JvmStatic
        @BindingAdapter("dateLong")
        fun setFormattedDate(textView: TextView, date: Long) {
            val sdf = java.text.SimpleDateFormat("HH:mm dd MM yyyy", Locale.getDefault())
            sdf.timeZone = TimeZone.getTimeZone("UTC")
            val dateStr = "${sdf.format(Date(date * 1000))} UTC"
            textView.text = dateStr
        }

        @JvmStatic
        @BindingAdapter("payloadsArray")
        fun setPayloads(textView: TextView, payloads: List<Payload>?) {
            if (payloads == null) {
                textView.visibility = GONE
                return
            } else textView.visibility = VISIBLE
            val s = StringBuffer()
            s.append("Payload: ")
            for (i in payloads.indices) {
                val p = payloads[i]
                s.append(p.payload_type)
                s.append(" for ")
                for (c in p.customers) {
                    s.append(c)
                    s.append(", ")
                }
                s.delete(s.length - 2, s.length - 1)
                if (i != payloads.size - 1) s.append(";\n")
            }
            textView.text = s.toString()
        }

        @JvmStatic
        @BindingAdapter("urlOnClick")
        fun onUrlClick(textView: TextView, url: String?) {
            textView.setOnClickListener {
                if (url != null && url.isNotBlank()) {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    textView.context.startActivity(i)
                }
            }
        }

        @JvmStatic
        @BindingAdapter("months")
        fun months(cev: ChartElementView, data: List<Pair<String, Int>>) {
            cev.setList(data)
        }

    }

}