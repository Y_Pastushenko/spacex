package com.aeolus.spacex.ui.main.main_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [MainListModule.ProvideViewModel::class])
abstract class MainListModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): MainListFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(MainListViewModel::class)
        fun provideViewModel(
            networkManager: NetworkManager,
            sharedData: SharedData
        ): ViewModel =
            MainListViewModel(networkManager, sharedData)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: MainListFragment
        ) = ViewModelProviders.of(target, factory).get(MainListViewModel::class.java)
    }

}