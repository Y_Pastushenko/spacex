package com.aeolus.spacex.ui.main.main_list

import com.aeolus.spacex.R
import com.aeolus.spacex.entities.launch.Launch
import com.aeolus.spacex.root.rv.RootRVAdapter

class LaunchesAdapter(
    onClickListener: ((Launch) -> Unit)? = null,
    onBottomReachedListener: (() -> Unit)? = null
) :
    RootRVAdapter<Launch>(onClickListener, onBottomReachedListener) {
    override fun getItemViewType(position: Int) = R.layout.list_item_launch
}