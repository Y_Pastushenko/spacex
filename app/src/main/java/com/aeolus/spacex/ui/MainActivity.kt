package com.aeolus.spacex.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.aeolus.spacex.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}