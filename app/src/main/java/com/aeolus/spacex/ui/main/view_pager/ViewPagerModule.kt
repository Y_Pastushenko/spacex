package com.aeolus.spacex.ui.main.view_pager

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ViewPagerModule.ProvideViewModel::class])
abstract class ViewPagerModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): ViewPagerFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(ViewPagerViewModel::class)
        fun provideViewModel(): ViewModel = ViewPagerViewModel()
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: ViewPagerFragment
        ) = ViewModelProviders.of(target, factory).get(ViewPagerViewModel::class.java)
    }

}