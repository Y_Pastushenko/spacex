package com.aeolus.spacex.ui.main.chart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ChartModule.ProvideViewModel::class])
abstract class ChartModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): ChartFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(ChartViewModel::class)
        fun provideViewModel(
            networkManager: NetworkManager,
            sharedData: SharedData
        ): ViewModel =
            ChartViewModel(networkManager,sharedData)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: ChartFragment
        ) = ViewModelProviders.of(target, factory).get(ChartViewModel::class.java)
    }

}