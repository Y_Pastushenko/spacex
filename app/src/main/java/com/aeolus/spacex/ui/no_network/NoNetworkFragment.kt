package com.aeolus.spacex.ui.no_network

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import com.aeolus.spacex.databinding.FragmentNoNetworkBinding
import com.aeolus.spacex.entities.event_bus.InternalDestination
import com.aeolus.spacex.entities.event_bus.SplashScreen
import com.aeolus.spacex.root.DestinationAwareFragment
import javax.inject.Inject


class NoNetworkFragment : DestinationAwareFragment() {

    @Inject
    lateinit var viewModel: NoNetworkViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                viewModel.onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentNoNetworkBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onNewDestination(eventOn: InternalDestination) {
        when {
            (eventOn is SplashScreen) -> navController?.navigate(NoNetworkFragmentDirections.openSplashScreen())
        }
    }

}