package com.aeolus.spacex.ui.no_network

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.aeolus.spacex.entities.event_bus.DestinationHome
import com.aeolus.spacex.entities.event_bus.SplashScreen
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.root.RootViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus

class NoNetworkViewModel(private val networkManager: NetworkManager) : RootViewModel() {

    val loadingVisible = MutableLiveData<Boolean>()

    private var backClickedBefore: Boolean = false

    init {
        loadingVisible.postValue(false)
    }

    fun onRetry() {
        loadingVisible.postValue(true)
        disposable.add(
            networkManager.getLaunches(
                1,
                0
            ).observeOn(AndroidSchedulers.mainThread()).subscribe { t1, t2 ->
                if (t2 == null && t1.isNotEmpty())
                    EventBus.getDefault().post(SplashScreen())
                else
                    loadingVisible.postValue(false)
            })
    }

    fun onBackPressed() {
        if (backClickedBefore) {
            EventBus.getDefault().post(DestinationHome())
        }
        backClickedBefore = true
        Handler().postDelayed({ backClickedBefore = false }, 2000)

    }

}