package com.aeolus.spacex.ui.details

import androidx.lifecycle.MutableLiveData
import com.aeolus.spacex.entities.launch.Launch
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.root.RootViewModel
import io.reactivex.schedulers.Schedulers

class DetailsViewModel(val networkManager: NetworkManager) : RootViewModel() {

    val selectedLaunch = MutableLiveData<Launch>()
    val toastToShow = MutableLiveData<String>()

    fun onLaunchDataGot(launch: Launch){
        selectedLaunch.postValue(launch)
    }

    fun onDownloadImageClick(url: String) {
        toastToShow.postValue("Image will be saved to Downloads folder")
        disposable.add(networkManager.downloadImage(url).observeOn(Schedulers.io()).subscribe {})
    }

}