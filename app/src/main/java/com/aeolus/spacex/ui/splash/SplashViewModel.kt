package com.aeolus.spacex.ui.splash

import com.aeolus.spacex.entities.event_bus.MainListScreen
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import com.aeolus.spacex.root.RootViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus

class SplashViewModel(
    networkManager: NetworkManager,
    sharedData: SharedData
) : RootViewModel() {

    init {
        disposable.add(
            networkManager.getLaunches(
                20,
                0
            ).observeOn(AndroidSchedulers.mainThread()).subscribe { t1, t2 ->
                if (t2 == null && t1.isNotEmpty()) {
                    sharedData.clearLaunches()
                    sharedData.addLaunches(t1)
                    EventBus.getDefault().post(MainListScreen())
                } else
                    EventBus.getDefault().post(NoNetworkScreen())
            })
    }

}