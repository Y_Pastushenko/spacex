package com.aeolus.spacex.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [SplashModule.ProvideViewModel::class])
abstract class SplashModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): SplashFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(SplashViewModel::class)
        fun provideViewModel(
            networkManager: NetworkManager,
            sharedData: SharedData
        ): ViewModel =
            SplashViewModel(networkManager, sharedData)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: SplashFragment
        ) = ViewModelProviders.of(target, factory).get(SplashViewModel::class.java)
    }

}