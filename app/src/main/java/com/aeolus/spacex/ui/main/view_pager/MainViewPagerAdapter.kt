package com.aeolus.spacex.ui.main.view_pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.aeolus.spacex.ui.main.chart.ChartFragment
import com.aeolus.spacex.ui.main.main_list.MainListFragment

class MainViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(
    fm,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {

    companion object {
        const val MAIN_LIST_POSITION = 0
        const val CHART_POSITION = 1
    }

    override fun getCount(): Int = 2

    override fun getItem(position: Int): Fragment {
        return when (position) {
            MAIN_LIST_POSITION -> MainListFragment()
            CHART_POSITION -> ChartFragment()
            else -> Fragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            MAIN_LIST_POSITION -> "List"
            CHART_POSITION -> "Chart"
            else -> ""
        }
    }

}