package com.aeolus.spacex.ui.main.chart

import androidx.lifecycle.MutableLiveData
import com.aeolus.spacex.entities.ChartElementData
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import com.aeolus.spacex.entities.launch.Launch
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import com.aeolus.spacex.root.RootViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus
import java.text.ParseException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChartViewModel(
    private val networkManager: NetworkManager,
    private val sharedData: SharedData
) : RootViewModel() {
    val launchesToShow = MutableLiveData<List<ChartElementData>>()
    val loadingVisible = MutableLiveData<Boolean>()

    init {
        launchesToShow.postValue(getDataForChart(groupByMonth(sharedData.getLaunches())))
    }

    fun onEndReached() {
        loadingVisible.postValue(true)
        disposable.add(
            networkManager
                .getLaunches(LAUNCHES_LIMIT, sharedData.getLaunches().size)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { t1, t2 ->
                    if (t2 == null && t1.isNotEmpty()) {
                        sharedData.addLaunches(t1)
                        launchesToShow.postValue(getDataForChart(groupByMonth(sharedData.getLaunches())))
                        loadingVisible.postValue(false)
                    } else
                        EventBus.getDefault().post(NoNetworkScreen())
                }
        )
    }

    private fun getDataForChart(l: List<Pair<String, Int>>): List<ChartElementData> {
        val list = ArrayList(l)
        list.removeAt(list.size - 1)
        val res = ArrayList<ChartElementData>()
        var i = 0
        var tList: ArrayList<Pair<String, Int>> = arrayListOf()
        var j = 0
        if (list.size >= MONTHS_PER_VIEW)
            while (true) {
                tList.add(list[i])
                i++
                j++
                if (j == MONTHS_PER_VIEW) {
                    j = 0
                    res.add(ChartElementData(tList))
                    tList = arrayListOf()
                    i--
                    if (list.size - i < MONTHS_PER_VIEW)
                        break
                }
            }
        return res
    }

    private fun groupByMonth(list: List<Launch>): List<Pair<String, Int>> {
        val l = HashMap<String, ArrayList<Launch>>()
        val sdf = java.text.SimpleDateFormat("MM-yyyy", Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        for (launch in list) {
            val key = sdf.format(Date(launch.launch_date_unix * 1000))
            var aL = l[key]
            if (aL == null) aL = arrayListOf()
            aL.add(launch)
            l[key] = aL
        }
        var keys = l.keys.toList()
        keys = keys.sortedBy { sdf.parse(it) }

        val beginCalendar = Calendar.getInstance()
        val finishCalendar = Calendar.getInstance()
        try {
            beginCalendar.time = sdf.parse(keys[0])!!
            finishCalendar.time = sdf.parse(keys[keys.size - 1])!!
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val dates = arrayListOf<String>()
        while (beginCalendar.before(finishCalendar)) {
            val date = sdf.format(beginCalendar.time)
            dates.add(date)
            beginCalendar.add(Calendar.MONTH, 1)
        }
        dates.add(keys[keys.size - 1])
        val res = arrayListOf<Pair<String, Int>>()
        for (date in dates) {
            if (l.containsKey(date))
                res.add(Pair(date, l[date]!!.size))
            else
                res.add(Pair(date, 0))
        }
        return res
    }

    companion object {
        const val LAUNCHES_LIMIT = 10
        const val MONTHS_PER_VIEW = 4
    }

}