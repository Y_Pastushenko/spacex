package com.aeolus.spacex.ui.no_network

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [NoNetworkModule.ProvideViewModel::class])
abstract class NoNetworkModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): NoNetworkFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(NoNetworkViewModel::class)
        fun provideViewModel(networkManager: NetworkManager): ViewModel =
            NoNetworkViewModel(networkManager)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: NoNetworkFragment
        ) = ViewModelProviders.of(target, factory).get(NoNetworkViewModel::class.java)
    }

}