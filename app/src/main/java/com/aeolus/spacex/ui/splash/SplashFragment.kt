package com.aeolus.spacex.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import com.aeolus.spacex.databinding.FragmentSplashBinding
import com.aeolus.spacex.entities.event_bus.InternalDestination
import com.aeolus.spacex.entities.event_bus.MainListScreen
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import com.aeolus.spacex.root.DestinationAwareFragment
import javax.inject.Inject


class SplashFragment : DestinationAwareFragment() {

    @Inject
    lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {}
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSplashBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onNewDestination(eventOn: InternalDestination) {
        when {
            (eventOn is MainListScreen) -> navController?.navigate(SplashFragmentDirections.openViepager())
            (eventOn is NoNetworkScreen) -> navController?.navigate(SplashFragmentDirections.splashOpenNoNetwork())
        }
    }

}