package com.aeolus.spacex.ui.main.main_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.spacex.databinding.FragmentMainListBinding
import com.aeolus.spacex.root.RootFragment
import kotlinx.android.synthetic.main.fragment_main_list.view.*
import javax.inject.Inject

class MainListFragment : RootFragment() {

    private val launchesAdapter = LaunchesAdapter({
        viewModel.onLaunchSelected(it)
    }, {
        viewModel.onBottomReached()
    })
    private var rv: RecyclerView? = null
    private var savedPosition: Int = -1

    @Inject
    lateinit var viewModel: MainListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentMainListBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.adapter = launchesAdapter
        binding.lifecycleOwner = this
        rv = binding.root.rv_main
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedPosition = savedInstanceState?.getInt(POSITION_KEY, -1) ?: -1
    }

    override fun onResume() {
        super.onResume()
        viewModel.launchesToShow.observe(this, Observer {
            launchesAdapter.submitList(it)
            if (savedPosition >= 0) {
                rv?.scrollToPosition(savedPosition)
                savedPosition = -1
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (rv == null) return
        val lm = rv!!.layoutManager as LinearLayoutManager
        var pos = lm.findLastCompletelyVisibleItemPosition()
        if (pos < 0) pos = lm.findFirstVisibleItemPosition()
        outState.putInt(POSITION_KEY, pos)
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val POSITION_KEY = "com.aeolus.spacex.ui.main.main_list.POSITION_KEY"
    }

}