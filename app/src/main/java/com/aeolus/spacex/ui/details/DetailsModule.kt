package com.aeolus.spacex.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.aeolus.spacex.di.ViewModelKey
import com.aeolus.spacex.model.network.NetworkManager
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [DetailsModule.ProvideViewModel::class])
abstract class DetailsModule {

    @ContributesAndroidInjector(modules = [InjectViewModel::class])
    abstract fun bind(): DetailsFragment

    @Module
    class ProvideViewModel {

        @Provides
        @IntoMap
        @ViewModelKey(DetailsViewModel::class)
        fun provideViewModel(networkManager: NetworkManager): ViewModel = DetailsViewModel(networkManager)
    }

    @Module
    class InjectViewModel {

        @Provides
        fun provideViewModel(
            factory: ViewModelProvider.Factory,
            target: DetailsFragment
        ) = ViewModelProviders.of(target, factory).get(DetailsViewModel::class.java)
    }

}