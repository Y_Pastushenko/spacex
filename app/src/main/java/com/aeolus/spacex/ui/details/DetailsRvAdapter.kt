package com.aeolus.spacex.ui.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.spacex.R
import com.aeolus.spacex.entities.launch.Launch

class DetailsRvAdapter(private val onImageClickListener: OnImageClickListener?) :
    ListAdapter<Launch, DetailsRvAdapter.DetailsVH>(DiffCallback<Launch>()) {

    fun setLaunch(launch: Launch) {
        submitList(listOf(launch))
    }

    override fun getItemCount(): Int {
        if (currentList.isEmpty()) return 0
        return currentList[0].links.flickr_images.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> R.layout.list_item_detailed_main
            else -> R.layout.list_item_detailed_image
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsVH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return DetailsVH(binding, onImageClickListener)
    }

    class DiffCallback<V> : DiffUtil.ItemCallback<V>() {
        override fun areItemsTheSame(oldItem: V, newItem: V): Boolean {
            return if (oldItem != null && newItem != null)
                oldItem == newItem
            else
                ((oldItem == null) == (newItem == null))
        }

        override fun areContentsTheSame(oldItem: V, newItem: V): Boolean {
            return areItemsTheSame(oldItem, newItem)
        }

    }

    override fun onBindViewHolder(holder: DetailsVH, position: Int) {
        holder.bind(getItem(position), if (position == 0) position else position - 1)
    }

    override fun getItem(position: Int): Launch {
        return currentList[0]
    }

    class DetailsVH(
        val binding: ViewDataBinding,
        private val onClickListener: OnImageClickListener?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Launch, position: Int) {
            binding.setVariable(BR.item, item)
            binding.setVariable(BR.position, position)
            if (onClickListener != null)
                binding.setVariable(BR.imageClickListener, onClickListener)
            binding.executePendingBindings()
        }

    }

    interface OnImageClickListener {
        fun onImageClick(url: String)
    }

}