package com.aeolus.spacex.ui.main.view_pager

import android.os.Handler
import com.aeolus.spacex.entities.event_bus.ChartScreen
import com.aeolus.spacex.entities.event_bus.DestinationHome
import com.aeolus.spacex.entities.event_bus.InternalDestination
import com.aeolus.spacex.entities.event_bus.MainListScreen
import com.aeolus.spacex.root.RootViewModel
import org.greenrobot.eventbus.EventBus


class ViewPagerViewModel() : RootViewModel() {

    private var backClickedBefore: Boolean = false

    fun onBackPressed(currentScreen: InternalDestination) {
        when (currentScreen) {
            is MainListScreen -> {
                if (backClickedBefore) {
                    EventBus.getDefault().post(DestinationHome())
                }
                backClickedBefore = true
                Handler().postDelayed({
                    backClickedBefore = false
                }, 2000)
            }
            is ChartScreen -> EventBus.getDefault().post(MainListScreen())
        }
    }

}