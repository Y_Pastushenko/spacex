package com.aeolus.spacex.ui.details

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.aeolus.spacex.databinding.FragmentDetailsBinding
import com.aeolus.spacex.entities.event_bus.InternalDestination
import com.aeolus.spacex.entities.event_bus.MainListScreen
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import com.aeolus.spacex.root.DestinationAwareFragment
import javax.inject.Inject


class DetailsFragment : DestinationAwareFragment() {

    private var rv: RecyclerView? = null
    private var savedPosition: Int = -1
    private val onDownloadImageListener: (String) -> Unit = { viewModel.onDownloadImageClick(it) }
    private val adapter = DetailsRvAdapter(object : DetailsRvAdapter.OnImageClickListener {
        override fun onImageClick(url: String) {
            val df = DownloadImageDF()
            df.setUrl(url)
            df.setListener(onDownloadImageListener)
            df.show(childFragmentManager, DF_DOWNLOAD_TAG)
        }
    })
    private val args: DetailsFragmentArgs by navArgs()

    @Inject
    lateinit var viewModel: DetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onLaunchDataGot(args.selectedLaunch)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bindings = FragmentDetailsBinding.inflate(inflater, container, false)
        bindings.adapter = adapter
        bindings.onBackClickListener = View.OnClickListener {
            activity?.onBackPressed()
        }
        bindings.lifecycleOwner = this
        return bindings.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedPosition = savedInstanceState?.getInt(POSITION_KEY, -1) ?: -1
    }

    override fun onResume() {
        super.onResume()
        viewModel.selectedLaunch.observe(this, Observer {
            adapter.setLaunch(it)
            if (savedPosition >= 0) {
                rv?.scrollToPosition(savedPosition)
                savedPosition = -1
            }
        })
        viewModel.toastToShow.observe(this, Observer {
            if (it.isNotBlank()) {
                showToast(it)
                viewModel.toastToShow.value = ""
            }
        })
        val f = childFragmentManager.findFragmentByTag(DF_DOWNLOAD_TAG)
        if (f != null && f is DownloadImageDF) {
            f.setListener(onDownloadImageListener)
        }
    }

    override fun onNewDestination(eventOn: InternalDestination) {
        when {
            (eventOn is MainListScreen) -> navController?.navigate(DetailsFragmentDirections.returnToViewPager())
            (eventOn is NoNetworkScreen) -> navController?.navigate(DetailsFragmentDirections.detailsOpenNoNetwork())
        }
    }

    companion object {

        const val POSITION_KEY = "com.aeolus.spacex.ui.details.POSITION_KEY"
        const val DF_DOWNLOAD_TAG = "com.aeolus.spacex.ui.details.DF_DOWNLOAD_TAG"

        class DownloadImageDF : DialogFragment() {

            private val urlKey = "com.aeolus.spacex.ui.details.URL_KEY"

            private var url: String? = null
            private var onApproved: ((String) -> Unit)? = null

            fun setUrl(url: String) {
                this.url = url
            }

            fun setListener(onApproved: (String) -> Unit) {
                this.onApproved = onApproved
            }

            override fun onCreate(savedInstanceState: Bundle?) {
                super.onCreate(savedInstanceState)
                val s = savedInstanceState?.getString(urlKey, "")
                if (s != null && s.isNotBlank()) url = s
            }

            override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
                return AlertDialog.Builder(context!!)
                    .setTitle("Save image to downloads?")
                    .setPositiveButton("Yes") { _, _ ->
                        if (url != null) {
                            onApproved?.invoke(url!!)
                        }
                    }
                    .setNegativeButton("No") { _, _ -> dismiss() }
                    .create()
            }

            override fun onSaveInstanceState(outState: Bundle) {
                outState.putString(urlKey, url)
                super.onSaveInstanceState(outState)
            }

        }
    }
}