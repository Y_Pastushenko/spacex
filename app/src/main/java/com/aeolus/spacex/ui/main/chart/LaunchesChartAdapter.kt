package com.aeolus.spacex.ui.main.chart

import com.aeolus.spacex.R
import com.aeolus.spacex.entities.ChartElementData
import com.aeolus.spacex.entities.launch.Launch
import com.aeolus.spacex.root.rv.RootRVAdapter

class LaunchesChartAdapter(
    onClickListener: ((ChartElementData) -> Unit)? = null,
    onBottomReachedListener: (() -> Unit)? = null
) : RootRVAdapter<ChartElementData>(onClickListener, onBottomReachedListener) {
    override fun getItemViewType(position: Int) = R.layout.list_item_chart
}