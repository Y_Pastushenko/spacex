package com.aeolus.spacex.ui.main.main_list

import androidx.lifecycle.MutableLiveData
import com.aeolus.spacex.entities.event_bus.DetailsScreen
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import com.aeolus.spacex.entities.launch.Launch
import com.aeolus.spacex.model.network.NetworkManager
import com.aeolus.spacex.model.shared_data.SharedData
import com.aeolus.spacex.root.RootViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus

class MainListViewModel(
    private val networkManager: NetworkManager,
    private val sharedData: SharedData
) : RootViewModel() {

    val launchesToShow = MutableLiveData<List<Launch>>()
    val loadingVisible = MutableLiveData<Boolean>()

    init {
        launchesToShow.postValue(sharedData.getLaunches())
    }

    fun onLaunchSelected(launch: Launch) {
        EventBus.getDefault().post(DetailsScreen(launch))
    }

    fun onBottomReached() {
        loadingVisible.postValue(true)
        disposable.add(
            networkManager
                .getLaunches(LAUNCHES_LIMIT, sharedData.getLaunches().size)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { t1, t2 ->
                    if (t2 == null && t1.isNotEmpty()) {
                        sharedData.addLaunches(t1)
                        launchesToShow.postValue(sharedData.getLaunches())
                        loadingVisible.postValue(false)
                    } else
                        EventBus.getDefault().post(NoNetworkScreen())
                }
        )
    }

    companion object {
        const val LAUNCHES_LIMIT = 10
    }

}