package com.aeolus.spacex.ui.main.view_pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.viewpager.widget.ViewPager
import com.aeolus.spacex.databinding.FragmentViewPagerBinding
import com.aeolus.spacex.entities.event_bus.*
import com.aeolus.spacex.root.DestinationAwareFragment
import kotlinx.android.synthetic.main.fragment_view_pager.view.*
import javax.inject.Inject

class ViewPagerFragment : DestinationAwareFragment() {

    private var viewPagerAdapter: MainViewPagerAdapter? = null

    private var viewPager: ViewPager? = null

    @Inject
    lateinit var viewModel: ViewPagerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewPagerAdapter = MainViewPagerAdapter(childFragmentManager)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (viewPager != null)
                    viewModel.onBackPressed(
                        if (viewPager!!.currentItem == MainViewPagerAdapter.MAIN_LIST_POSITION)
                            MainListScreen()
                        else
                            ChartScreen()
                    )
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        viewPager = binding.root.view_pager_main
        binding.root.tab_layout_main.setupWithViewPager(viewPager)
        binding.adapter = viewPagerAdapter
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onNewDestination(eventOn: InternalDestination) {
        when {
            (eventOn is MainListScreen) -> viewPager?.currentItem = MainViewPagerAdapter.MAIN_LIST_POSITION
            (eventOn is ChartScreen) -> viewPager?.currentItem = MainViewPagerAdapter.CHART_POSITION
            (eventOn is DetailsScreen) -> navController?.navigate(ViewPagerFragmentDirections.openDetails(eventOn.launch))
            (eventOn is NoNetworkScreen) -> navController?.navigate(ViewPagerFragmentDirections.vpOpenNoNetwork())
        }
    }

}