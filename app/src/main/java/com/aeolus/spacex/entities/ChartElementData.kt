package com.aeolus.spacex.entities

data class ChartElementData(val months: List<Pair<String, Int>>)