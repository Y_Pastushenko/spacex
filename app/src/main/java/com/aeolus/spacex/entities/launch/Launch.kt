package com.aeolus.spacex.entities.launch

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Launch(
    val details: String,
    val flight_number: Int,
    val launch_date_unix: Long,
    val launch_site: LaunchSite,
    val launch_success: Boolean,
    val launch_year: String,
    val links: Links,
    val mission_id: List<String>,
    val mission_name: String,
    val rocket: Rocket,
    val ships: List<String>,
    val upcoming: Boolean
): Parcelable