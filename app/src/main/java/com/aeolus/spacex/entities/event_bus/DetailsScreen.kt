package com.aeolus.spacex.entities.event_bus

import com.aeolus.spacex.entities.launch.Launch

data class DetailsScreen(val launch: Launch) : InternalDestination()
