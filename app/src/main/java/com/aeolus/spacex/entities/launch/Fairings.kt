package com.aeolus.spacex.entities.launch

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fairings(
    val recovered: Boolean,
    val recovery_attempt: Boolean,
    val reused: Boolean
): Parcelable