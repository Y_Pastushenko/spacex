package com.aeolus.spacex.entities.launch

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
): Parcelable