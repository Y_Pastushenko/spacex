package com.aeolus.spacex.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aeolus.spacex.entities.event_bus.SplashScreen
import com.aeolus.spacex.root.RootViewModelFactory
import com.aeolus.spacex.ui.main.chart.ChartModule
import com.aeolus.spacex.ui.no_network.NoNetworkModule
import com.aeolus.spacex.ui.details.DetailsModule
import com.aeolus.spacex.ui.main.main_list.MainListModule
import com.aeolus.spacex.ui.main.view_pager.ViewPagerModule
import com.aeolus.spacex.ui.splash.SplashModule
import dagger.Module
import dagger.Provides
import javax.inject.Provider

@Module(
    includes = [
        SplashModule::class,
        DetailsModule::class,
        MainListModule::class,
        NoNetworkModule::class,
        ChartModule::class,
        ViewPagerModule::class
    ]
)
class UiModule {

    @Provides
    fun provideViewModelFactory(
        providers: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
    ): ViewModelProvider.Factory =
        RootViewModelFactory(providers)
}