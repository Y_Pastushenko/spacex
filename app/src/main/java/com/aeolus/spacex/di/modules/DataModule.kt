package com.aeolus.spacex.di.modules

import android.app.Application
import com.aeolus.spacex.model.network.*
import com.aeolus.spacex.model.shared_data.SharedData
import com.aeolus.spacex.model.shared_data.SharedDataImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideRetrofit(): RetrofitService {
        return Retrofit.makeRetrofitService()
    }

    @Provides
    @Singleton
    fun provideDownloadManager(application: Application): AppDownloadManager {
        return AppDownloadManager(application)
    }

    @Provides
    @Singleton
    fun provideNetworkManager(
        retrofitService: RetrofitService,
        downloadManager: AppDownloadManager
    ): NetworkManager {
        return NetworkManagerImpl(retrofitService, downloadManager)
    }

    @Provides
    @Singleton
    fun providesharedData(): SharedData {
        return SharedDataImpl()
    }

}