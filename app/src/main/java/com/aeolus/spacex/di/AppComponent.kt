package com.aeolus.spacex.di

import com.aeolus.spacex.App
import com.aeolus.spacex.di.modules.AndroidModule
import com.aeolus.spacex.di.modules.DataModule
import com.aeolus.spacex.di.modules.UiModule
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidModule::class,
        DataModule::class,
        AndroidSupportInjectionModule::class,
        UiModule::class
    ]
)
@Singleton
interface AppComponent {

    fun inject(app: App)

}