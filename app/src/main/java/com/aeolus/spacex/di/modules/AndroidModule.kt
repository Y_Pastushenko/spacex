package com.aeolus.spacex.di.modules

import android.app.Application
import android.content.Context
import com.aeolus.spacex.App
import dagger.Module
import dagger.Provides

@Module
class AndroidModule(private val application: App) {

    @Provides
    fun provideContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun provideApplication(): Application {
        return application
    }

}
