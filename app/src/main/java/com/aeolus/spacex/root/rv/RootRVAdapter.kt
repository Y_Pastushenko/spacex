package com.aeolus.spacex.root.rv

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



abstract class RootRVAdapter<T>(
    private val onClickListener: ((T) -> Unit)? = null,
    private val onBottomReachedListener: (() -> Unit)? = null
) : ListAdapter<T, RootRVViewHolder<T>>(DiffCallback<T>()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RootRVViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return RootRVViewHolder(binding, onClickListener)
    }

    override fun onBindViewHolder(holder: RootRVViewHolder<T>, position: Int) {
        if (position == itemCount - 1)
            onBottomReachedListener?.invoke()
        holder.bind(getItem(position))
    }

    override fun submitList(list: List<T>?) {
        super.submitList(if (list != null) ArrayList(list) else null)
    }

    class DiffCallback<V> : DiffUtil.ItemCallback<V>() {
        override fun areItemsTheSame(oldItem: V, newItem: V): Boolean {
            return if (oldItem != null && newItem != null)
                oldItem == newItem
            else
                ((oldItem == null) == (newItem == null))
        }

        override fun areContentsTheSame(oldItem: V, newItem: V): Boolean {
            return areItemsTheSame(oldItem, newItem)
        }

    }
}