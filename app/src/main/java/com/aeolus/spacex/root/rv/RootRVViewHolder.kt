package com.aeolus.spacex.root.rv

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView

class RootRVViewHolder<T>(
    private val binding: ViewDataBinding,
    private val onClickListener: ((T) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: T) {
        binding.setVariable(BR.item, item)
        if (onClickListener != null)
            binding.setVariable(BR.onClickListener, View.OnClickListener { onClickListener!!(item) })
        binding.executePendingBindings()
    }

}