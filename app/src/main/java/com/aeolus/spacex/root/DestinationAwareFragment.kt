package com.aeolus.spacex.root

import com.aeolus.spacex.entities.event_bus.DestinationHome
import com.aeolus.spacex.entities.event_bus.InternalDestination
import com.aeolus.spacex.entities.event_bus.NoNetworkScreen
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class DestinationAwareFragment : RootFragment() {

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    open fun onHome(eventOn: DestinationHome) {
        exitApp()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onNewDestination(eventOn: InternalDestination) {

    }

}