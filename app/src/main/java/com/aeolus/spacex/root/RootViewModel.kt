package com.aeolus.spacex.root

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import org.greenrobot.eventbus.EventBus

abstract class RootViewModel : ViewModel() {

    protected val disposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
        EventBus.getDefault().unregister(this)
    }

}