package com.aeolus.spacex.model.network

import com.aeolus.spacex.entities.launch.Launch
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class NetworkManagerImpl(
    private val retrofitService: RetrofitService,
    private val appDownloadManager: AppDownloadManager
) : NetworkManager {

    override fun getLaunches(limit: Int, offset: Int): Single<List<Launch>> {
        return retrofitService.getLaunches(limit, offset).subscribeOn(Schedulers.io())
    }

    override fun downloadImage(url: String): Completable {
        val action = { appDownloadManager.startDownload(url) }
        return Completable.fromAction(action).subscribeOn(Schedulers.io())
    }

}