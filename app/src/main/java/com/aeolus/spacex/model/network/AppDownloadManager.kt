package com.aeolus.spacex.model.network

import android.app.Application
import android.app.DownloadManager
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Environment.DIRECTORY_DOWNLOADS


class AppDownloadManager(application: Application) {

    private var dm: DownloadManager? = null

    init {
        dm = application.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
    }

    fun startDownload(url: String) {
        dm?.enqueue(
            DownloadManager.Request(Uri.parse(url))
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle("Image")
                .setDestinationInExternalPublicDir(
                    DIRECTORY_DOWNLOADS,
                    url.substring(url.lastIndexOf('/' + 1))
                )
        )
    }
}