package com.aeolus.spacex.model.shared_data

import com.aeolus.spacex.entities.launch.Launch

class SharedDataImpl : SharedData {

    private val launches = arrayListOf<Launch>()

    @Synchronized
    override fun addLaunches(launches: List<Launch>) {
            this.launches.addAll(launches)
    }

    @Synchronized
    override fun getLaunches(): List<Launch> {
        return launches
    }

    @Synchronized
    override fun clearLaunches() {
        launches.clear()
    }

}