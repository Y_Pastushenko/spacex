package com.aeolus.spacex.model.shared_data

import com.aeolus.spacex.entities.launch.Launch

interface SharedData {
    fun addLaunches(launches: List<Launch>)
    fun getLaunches(): List<Launch>
    fun clearLaunches()
}