package com.aeolus.spacex.model.network

import com.aeolus.spacex.entities.launch.Launch
import io.reactivex.Completable
import io.reactivex.Single

interface NetworkManager {
    fun getLaunches(limit: Int, offset: Int): Single<List<Launch>>
    fun downloadImage(url: String): Completable
}