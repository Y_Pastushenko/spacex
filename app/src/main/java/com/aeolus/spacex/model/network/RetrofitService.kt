package com.aeolus.spacex.model.network

import com.aeolus.spacex.entities.launch.Launch
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {

    @GET("launches")
    fun getLaunches(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<List<Launch>>

}