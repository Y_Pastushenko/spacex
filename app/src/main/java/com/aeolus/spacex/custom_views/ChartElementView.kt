package com.aeolus.spacex.custom_views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.os.Handler
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.aeolus.spacex.R
import com.aeolus.spacex.Utils.Companion.dpToPx
import com.aeolus.spacex.Utils.Companion.spToPx
import kotlin.math.sqrt


class ChartElementView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var popUp: PopupWindow? = null
    private var currentWidth = 0f
    private var currentHeight = 0f
    private val gridLinePaint = Paint()
    private val chartLinePaint = Paint()
    private val circlePaint = Paint()
    private val textPaint = Paint()
    private var circleRadiusPx = 0f
    private var lineWidthPx = 0f
    private val months = arrayListOf<Pair<String, Int>>()
    private val map = hashMapOf<Pair<Float, Float>, Pair<String, Int>>()
    private val points = arrayListOf<Pair<Float, Float>>()
    private val path = Path()
    private val lines = arrayListOf<Pair<Pair<Float, Float>, Pair<Float, Float>>>()
    private val text = arrayListOf<Pair<String, Pair<Float, Float>>>()

    init {
        lineWidthPx = dpToPx(context, LINE_WIDTH_DP)
        circleRadiusPx = dpToPx(context, CIRCLE_RADIUS_DP)
        circlePaint.color = ContextCompat.getColor(context, R.color.colorAccent)
        circlePaint.style = Paint.Style.FILL_AND_STROKE
        gridLinePaint.color = Color.GRAY
        gridLinePaint.style = Paint.Style.STROKE
        gridLinePaint.strokeWidth = 1f
        chartLinePaint.color = ContextCompat.getColor(context, R.color.colorAccent)
        chartLinePaint.style = Paint.Style.STROKE
        chartLinePaint.strokeWidth = lineWidthPx
        textPaint.color = Color.BLACK
        textPaint.textSize = spToPx(context, TEXT_SIZE_SP)
    }


    override fun onSizeChanged(xNew: Int, yNew: Int, xOld: Int, yOld: Int) {
        super.onSizeChanged(xNew, yNew, xOld, yOld)
        currentWidth = xNew.toFloat()
        currentHeight = yNew.toFloat()
        calculateValues()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        calculateValues()
        for (line in lines)
            canvas?.drawLine(
                line.first.first,
                line.first.second,
                line.second.first,
                line.second.second,
                gridLinePaint
            )
        canvas?.drawPath(path, chartLinePaint)
        for (point in points)
            canvas?.drawCircle(point.first, point.second, circleRadiusPx, circlePaint)
        for (tp in text) {
            canvas?.save()
            canvas?.rotate(90f, tp.second.first, tp.second.second)
            canvas?.drawText(tp.first, tp.second.first, tp.second.second, textPaint)
            canvas?.restore()
        }
    }

    fun setList(list: List<Pair<String, Int>>) {
        months.clear()
        months.addAll(list)
        popUp = PopupWindow(context)
        setOnClickListener()
    }

    fun setOnClickListener() {
        var lastTouchDown: Long = 0
        setOnTouchListener { _: View, motionEvent: MotionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> lastTouchDown = System.currentTimeMillis()
                MotionEvent.ACTION_UP -> {
                    if (System.currentTimeMillis() - lastTouchDown < CLICK_ACTION_THRESHHOLD) {
                        val x = motionEvent.x
                        val y = motionEvent.y
                        val rPX = dpToPx(context, ON_CLICK_RADIUS)
                        for (point in points)
                            if (distance(x, y, point) < rPX) {
                                val t = map[point]
                                if (t != null) {
                                    showPopUp(t.first, t.second, x.toInt(), y.toInt())
                                }
                            }
                    }
                }
            }
            true
        }
    }

    private fun showPopUp(date: String, launches: Int, x: Int, y: Int) {
        popUp?.dismiss()
        popUp = PopupWindow(context)
        val textView = TextView(context)
        textView.setTextColor(Color.WHITE)
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        textView.layoutParams = ViewGroup.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        popUp!!.contentView = textView
        val text = "$launches launches at $date"
        textView.text = text
        popUp!!.showAsDropDown(this, x, y)
        Handler().postDelayed({
            popUp?.dismiss()
        }, POPUP_TTL)
    }

    private fun distance(x: Float, y: Float, point: Pair<Float, Float>): Float {
        return sqrt((x - point.first) * (x - point.first) + (y - point.second) * (y - point.second))
    }

    private fun calculateValues() {
        points.clear()
        lines.clear()
        text.clear()
        path.rewind()
        map.clear()
        popUp?.dismiss()
        popUp = null
        if (months.size < 2) return
        val columns = months.size - 1
        val columnsWidth = currentWidth / columns
        val unitHeight = currentHeight / Y_MAX
        var currentX = 0f
        for (i in 0 until months.size) {
            lines.add(Pair(Pair(currentX, 0f), Pair(currentX, currentHeight)))
            text.add(
                Pair(
                    months[i].first,
                    Pair(currentX + dpToPx(context, 5f), dpToPx(context, 20f))
                )
            )
            if (months[i].second < 0) continue
            val point = Pair(currentX, (Y_MAX - months[i].second - 1) * unitHeight)
            points.add(point)
            map[point] = months[i]
            currentX += columnsWidth
        }
        path.moveTo(points[0].first, points[0].second)
        for (i in 1 until points.size) {
            path.lineTo(points[i].first, points[i].second)
        }
        var currentY = 0f
        for (i in 0 until Y_MAX) {
            lines.add(Pair(Pair(0f, currentY), Pair(currentWidth, currentY)))
            currentY += unitHeight
        }
    }

    companion object {
        const val Y_MAX = 10
        const val POPUP_TTL = 1000L
        const val CIRCLE_RADIUS_DP = 8f
        const val LINE_WIDTH_DP = 1f
        const val TEXT_SIZE_SP = 16f
        const val ON_CLICK_RADIUS = 50f
        private const val CLICK_ACTION_THRESHHOLD = 200
    }

}