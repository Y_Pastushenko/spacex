package com.aeolus.spacex

import android.app.Application
import androidx.fragment.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import com.aeolus.spacex.di.AppComponent
import com.aeolus.spacex.di.DaggerAppComponent
import com.aeolus.spacex.di.modules.AndroidModule
import javax.inject.Inject

class App : Application(), HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    lateinit var component: AppComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .androidModule(AndroidModule(this))
            .build()
        component.inject(this)
    }

    override fun supportFragmentInjector() = fragmentInjector
}